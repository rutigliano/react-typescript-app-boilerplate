import * as React from "react";

function greeter(person: string) {
    return "This is, " + person + " from";
}
let user = "Domenico";

export interface DomenicoProps { compiler: string; framework: string; }

export const Domenico = (props: DomenicoProps) => <h1>  {greeter(user)} {props.compiler} and {props.framework}!</h1>;
