import * as React from "react";
import * as ReactDOM from "react-dom";

import { Domenico } from "./components/Domenico";

ReactDOM.render(
    <Domenico compiler="TypeScript" framework="React" />,
    document.getElementById("domApp")
);
